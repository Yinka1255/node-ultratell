import mongoose from 'mongoose';
const Schema = mongoose.Schema;
import passportLocalMongoose from 'passport-local-mongoose';

const User = new Schema({
    username: String,
    password: String,
    name: String,
    phone: String,
    role: Number, 
}, { collection: 'users' });

User.plugin(passportLocalMongoose);

export default mongoose.model('user', User);