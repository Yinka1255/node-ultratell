import mongoose from 'mongoose';
const Schema = mongoose.Schema;
import passportLocalMongoose from 'passport-local-mongoose';

const Todo = new Schema({
    userId: { type: Schema.Types.ObjectId, ref: 'users' },
    title: String,
    description: String,
    created: { type: Date, default: Date.now },
    completionDate: String,
    status: { type: Number, default: 1 }, //1 is active, 2 is deleted, 3 is completed
});


export default mongoose.model('todo', Todo);