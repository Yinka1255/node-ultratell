
import express from 'express';
import passport from 'passport';
import Todo from '../models/todo';
import { Types } from 'mongoose';
const router = express.Router();
const multer = require('multer');
var path = require('path');
var fs = require('fs');

router.post('/create_new', (req, res) => {
	var newTodo = new Todo({
		userId: req.body.userId,
		title: req.body.title,
		description: req.body.description,
		completionDate: req.body.completionDate,
	});
	newTodo.save(function(err, todo) {
		if (err){
			console.log(err);
			return res.json({'response': 'error', 'message':'Oops! An error occured.'});
		}  
		else{
			//sendPush();
			res.json({'response': 'success', 'message': 'Nice! todo has been saved'});
		}  
	});
});

router.post('/update', (req, res) => {
	Todo.findById(req.body._id).exec(function(err, todo) {
		todo.title = req.body.title;
		todo.description = req.body.description;
		todo.completionDate =  req.body.completionDate;
	
		todo.save(function(err, todo) {
			if (err){
				console.log(err);
				return res.json({'response': 'error', 'message':'Oops! An error occured.'});
			}  
			else{
				res.json({'response': 'success', 'message': 'Nice! todo has been saved'});
			}  
		});
	});
});

router.get('/get', function(req, res) {
	Todo.find({'status': { $ne: 2 }}).sort({ createdAt: -1 }).exec(function(err, todos) {
		return res.json({ 'todos': todos });
	  });
});

router.get('/delete/:todoId', function(req, res) {
	Todo.findById(req.params.todoId).exec(function(err, todo) {
		todo.status = 2;
		todo.save(function(err, todo) {
			if (err){
				console.log(err);
				return res.json({'response': 'error', 'message':'Oops! An error occured.'});
			}  
			else{
				//sendPush();
				res.json({'response': 'success', 'message': 'Nice! todo has been deleted'});
			}  
		});
	  });
  });

  router.get('/complete/:todoId', function(req, res) {
	Todo.findById(req.params.todoId).exec(function(err, todo) {
		todo.status = 3;
		todo.save(function(err, todo) {
			if (err){
				console.log(err);
				return res.json({'response': 'error', 'message':'Oops! An error occured.'});
			}  
			else{
				//sendPush();
				res.json({'response': 'success', 'message': 'Nice! todo has been completed'});
			}  
		});
	  });
  });

export default router;
