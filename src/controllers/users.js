
import express from 'express';
import passport from 'passport';
import User from '../models/user';
import { Types } from 'mongoose';
const router = express.Router();
const multer = require('multer');
var path = require('path');
var fs = require('fs');

router.get('/register', (req, res, next) => { 
	User.register(new User({ username: 'admin@ultraltell.com' }),
   'password', (err, user) => {

     if (err) {
       return res.json({error: 'Oops! An error occured. Looks like username already exist. Ensure you porovide a unique username'});
     }
     passport.authenticate('local')(req, res, () => {
       req.session.save((err) => {
	        if (err) {
	           return next(err);
	        }

					User.findById(user.id, function(err, user) {
						if (!user){
							console.log(err);
							//return res.json({'error': 'Sorry! We could not find your account'});
						}
						else {
							// do your updates here
							user.name = 'Benson wesby';
							user.phone = '08189862968';
							
							user.save(function(err, user){
							if(err)
									return res.json({error: 'Oops! An error occured.'});
							else
								return res.json({success: user});
		
							}); 
						}  
					});  
	    });      
     });
   });
});


router.get('/login', (req, res) => {
  res.render('users/login', { layout: false });
});

router.post('/login', function(req, res, next) { 
	passport.authenticate('local', function(err, user, info) {

    if(err)
		return res.json({ 'response': 'Error' });
		            
    if (!user) { return res.json({ 'response': 'error' }); }
    
    req.logIn(user, function(err) {
      
      if (err) { return res.json({ 'response': 'error' }); }

      	
		  return res.json({ 'response': 'Success', 'user': user  });        
    });

  })(req, res, next);
});


router.get('/logout', (req, res, next) => {
  req.logout();
  req.session.save((err) => {
    if (err) {
      return next(err);
    }
    res.redirect('/');
  });
});


export default router;
