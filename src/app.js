import dotenv from 'dotenv';
import express from 'express';
import path from 'path';
// import favicon from 'serve-favicon';
import logger from 'morgan';
import bodyParser from 'body-parser';
import lessMiddleware from 'less-middleware';
import passport from 'passport';
import mongoose from 'mongoose';
import { Strategy } from 'passport-local';
import cors from'cors';
// routes are imported here, note any auth or init middleware are to be placed
// above this line.

import users from './controllers/users';
import todos from './controllers/todos';



dotenv.config();
const blocks = {};
const app = express();


// uncomment after placing your favicon in /public
// app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(cors({credentials: true, origin: true}));
app.use(logger('dev'));
app.use(bodyParser.json({ limit: '50mb'}));
app.use(bodyParser.urlencoded({ extended: true, limit: '50mb'}));
app.use(lessMiddleware(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'public')));



app.use(passport.initialize());
app.use(express.static(path.join(__dirname, 'public')));


app.use('/users', users);
app.use('/todos', todos);
// passport account auth

import User from './models/user';

passport.use(new Strategy(User.authenticate()));
passport.serializeUser(User.serializeUser());
passport.deserializeUser(User.deserializeUser());

// mongoose
//mongoose.connect('mongodb://brims_001:Brims.001@brims-shard-00-00-fpuce.mongodb.net:27017,brims-shard-00-01-fpuce.mongodb.net:27017,brims-shard-00-02-fpuce.mongodb.net:27017/brims?ssl=true&replicaSet=brims-shard-0&authSource=admin');

mongoose.connect('mongodb://localhost:27017/ultra');


module.exports = app;
